APP_NAME = iShrink

QT += network sql
CONFIG += qt warn_on cascades10
LIBS += -lbbdata -lbbdevice -lbbsystem -lbbcascadesdatamanager -lbbplatformbbm -lbbplatform -lbb
CODECFORTR = UTF-8
CODECFORSRC = UTF-8
TRANSLATIONS = \
iShrink_sk.ts

INCLUDEPATH += ../src   SOURCES += ../src/*.cpp
HEADERS += ../src/*.h
 
device {
    CONFIG(release, debug|release) {
        DESTDIR = o.le-v7
        TEMPLATE=lib
        QMAKE_CXXFLAGS += -fvisibility=hidden
    }
    CONFIG(debug, debug|release) {
        DESTDIR = o.le-v7-g
    }
}
 
simulator {
    CONFIG(release, debug|release) {
        DESTDIR = o
    }
    CONFIG(debug, debug|release) { 
        DESTDIR = o-g
    } 
}
 
OBJECTS_DIR = $${DESTDIR}/.obj
MOC_DIR = $${DESTDIR}/.moc
RCC_DIR = $${DESTDIR}/.rcc
UI_DIR = $${DESTDIR}/.ui



include(config.pri)
