import bb.cascades 1.3

ActionItem {
    id: root
    
    property alias text: root.title
    property alias icon: root.imageSource
    
    signal clicked
    
    title: qsTr("Refresh") + Retranslate.onLocaleOrLanguageChanged
    imageSource: "asset:///images/refresh.png"
    onTriggered: {
        root.clicked()
    }
    shortcuts: [
        Shortcut {
            key: "R"
        }
    ]

}