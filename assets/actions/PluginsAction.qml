import bb.cascades 1.3

ActionItem {
    id: root
    
    property alias text: root.title
    property alias icon: root.imageSource
    
    signal clicked
    
    title: qsTr("Servers") + Retranslate.onLocaleOrLanguageChanged
    imageSource: "asset:///images/plugins.png"
    onTriggered: {
        root.clicked()
    }
    shortcuts: [
        Shortcut {
            key: "L"
        }
    ]
    
}