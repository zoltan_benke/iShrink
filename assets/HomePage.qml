import bb.cascades 1.3
import bb.data 1.0
import bb.system 1.2

import "components"
import "actions"
import "items"

BasePage {
    id: root
    
    function dropdown(){
        dropDown.removeAll();
        var list = PluginControl.installed
        console.log(list)
        var select = list.length === 1
        for (var item in list){
            var option = dynamicOption.createObject(dropDown)
            option.text = list[item].text
            option.description = list[item].site
            option.value = list[item].text
            if (Database.defaultPlugin == list[item].text){
                option.selected = true
            }
            dropDown.add(option)
        }
    }
    
    function learnRegExp(s) {    
        var myRegExp =/^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i;
        return myRegExp.test(s);
    }
    
    function setShortUrl(text){
        console.log(dropDown.selectedValue+" | "+text)
        indicator.stop()
        if (text == "Error"){
            Helper.showToast(qsTr("The url is blocked by URL.IE") + Retranslate.onLocaleOrLanguageChanged)
        }else{
            console.log("ShortUrl Done: "+text)
            shortUrl.text = text
        }
    }
    
    function createLink(){
        if (dropDown.selectedOption){
            urlValidator.validate()
            if (urlValidator.valid && urlField.text.toLowerCase().indexOf(dropDown.selectedOption.description.toLowerCase()) === -1){
                shortUrl.text = ""
                indicator.start()
                PluginControl.shortUrl(dropDown.selectedValue, urlField.text.trim())
            }
        }else{
            Helper.showToast(qsTr("Select server first") + Retranslate.onLocaleOrLanguageChanged)
        }
    }
    
    titleText: "iShrink"
    
    actions: [
        ActionItem {
            ActionBar.placement: ActionBarPlacement.OnBar
            title: qsTr("Create link") + Retranslate.onLocaleOrLanguageChanged
            imageSource: "asset:///images/shrink.png"
            onTriggered: {
                createLink()
            }
            shortcuts: [
                SystemShortcut {
                    type: SystemShortcuts.CreateNew
                }
            ]
        },
        PluginsAction {
            ActionBar.placement: ActionBarPlacement.OnBar
            title: qsTr("Servers") + Retranslate.onLocaleOrLanguageChanged
            onClicked: {
                var page = dynamicPlugins.createObject()
                pane.push(page)
            }
        }
    ]
    
    onCreationCompleted: {
        PluginControl.installedChanged.connect(dropdown)
        PluginControl.urlDone.connect(setShortUrl)
    }
    
    onActiveChanged: {
        if (active){
            dropdown()
        }
    }
    
    
    attachedObjects: [
        ComponentDefinition {
            id: dynamicOption
            Option {
            }
        },
        SystemToast {
            id: toast
            autoUpdateEnabled: true
        }
    ]
    
    
    
    ScrollView {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        scrollViewProperties{
            pinchToZoomEnabled: false
        }
        
        Container {
            topPadding: 20
            rightPadding: 20
            leftPadding: 20
            bottomPadding: 20
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            DropDown {
                id: dropDown
                title: qsTr("Active server") + Retranslate.onLocaleOrLanguageChanged
                onSelectedOptionChanged: {
                    if (selectedValue != "")
                        Database.defaultPlugin = selectedValue
                    if (shortUrl.text.length)
                        shortUrl.text = ""
                }
            }  
            TextField {
                id: urlField
                objectName: "urlField"
                horizontalAlignment: HorizontalAlignment.Center
                hintText: "url"
                textFormat: TextFormat.Auto
                inputMode: TextFieldInputMode.Url
                onCreationCompleted: {
                    urlField.requestFocus()
                }
                validator: Validator {
                    id: urlValidator
                    mode: ValidationMode.Immediate
                    errorMessage: qsTr("Invalid url") + Retranslate.onLocaleOrLanguageChanged
                    onValidate: {
                        valid = learnRegExp(urlField.text.trim())
                    }
                }
                input{
                    submitKey: SubmitKey.Submit
                    onSubmitted: {
                        createLink()
                    }
                }
            }
            Container {
                layout: DockLayout {}
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                ActivityIndicator {
                    id: indicator
                    preferredHeight: 150
                    preferredWidth: 150
                    horizontalAlignment: HorizontalAlignment.Center
                    verticalAlignment: VerticalAlignment.Center
                }
                Container {
                    layout: StackLayout {}
                    background: paint.imagePaint
                    attachedObjects: [
                        ImagePaintDefinition {
                            id: paint
                            imageSource: "asset:///images/image_border.png"
                        }
                    ]
                    visible: shortUrl.text != "" && !indicator.running
                    horizontalAlignment: HorizontalAlignment.Fill
                    verticalAlignment: VerticalAlignment.Fill
                    Label {
                        id: shortUrl
                        objectName: "shortUrlLabel"
                        horizontalAlignment: HorizontalAlignment.Center
                        multiline: true
                        textStyle{
                            fontSize: FontSize.PointValue
                            fontSizeValue: 11
                        }
                    } // end of ShortUrl label
                    Container {
                        bottomPadding: 15
                        topPadding: 10
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        horizontalAlignment: HorizontalAlignment.Center
                        Container {
                            rightPadding: 30
                            ImageButton {
                                id: copyBtn
                                defaultImageSource: "asset:///images/copy_blue.png"
                                pressedImageSource: defaultImageSource
                                onClicked: {
                                    Helper.copyText(shortUrl.text.trim())
                                }
                            }
                        }
                        
                        Container {
                            background: Color.Black
                            verticalAlignment: VerticalAlignment.Fill
                            horizontalAlignment: HorizontalAlignment.Left
                            maxHeight: Infinity                        
                            minWidth: 1
                            maxWidth: 1
                        }
                        Container {
                            leftPadding: 30
                            rightPadding: 30
                            ImageButton {
                                id: browserBtn
                                defaultImageSource: "asset:///images/open-browser_blue.png"
                                pressedImageSource: defaultImageSource
                                onClicked: {
                                    Helper.openBrowser(shortUrl.text.trim())
                                }
                            }
                        }
                        
                        Container {
                            background: Color.Black
                            verticalAlignment: VerticalAlignment.Fill
                            horizontalAlignment: HorizontalAlignment.Left
                            maxHeight: Infinity                        
                            minWidth: 1
                            maxWidth: 1
                        }
                        Container {
                            leftPadding: 30
                            ImageButton {
                                id: shareBtn
                                defaultImageSource: "asset:///images/share_blue.png"
                                pressedImageSource: defaultImageSource
                                onClicked: {
                                    var text = Database.shareText.trim() + " " + shortUrl.text.trim() + " " + pane.sharedText
                                    Helper.share(text.trim())
                                }
                            }
                        }
                    } // end of buttons container
                } // end of ShortUrl text Container
            }
        } // end of Root Container
    } // end of scrollView


}
