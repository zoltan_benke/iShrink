import bb.cascades 1.3

import "components"

BasePage {
    id: root
    
    titleText: qsTr("Settings") + Retranslate.onLocaleOrLanguageChanged
    
    onActiveChanged: {
        if (active){
            settingsAction.enabled = false
        }else{
            settingsAction.enabled = true
        }
    }
    
    ScrollView {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        scrollViewProperties{
            pinchToZoomEnabled: false
            scrollMode: ScrollMode.Vertical
        }
        
        
        Container {
            leftPadding: 20
            topPadding: 20
            bottomPadding: 20
            rightPadding: 20
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            
            DropDown {
                id: languageDropdown
                title: qsTr("Current language") + Retranslate.onLocaleOrLanguageChanged
                Option {
                    text: qsTr("English") + Retranslate.onLocaleOrLanguageChanged
                    value: "en"
                    selected: Database.language === value
                }
                
                Option {
                    text: qsTr("Chinese") + Retranslate.onLocaleOrLanguageChanged
                    value: "zh_CN"
                    selected: Database.language === value
                }
                
                Option {
                    text: qsTr("Czech") + Retranslate.onLocaleOrLanguageChanged
                    value: "cs"
                    selected: Database.language === value
                }
                
                Option {
                    text: qsTr("Slovak") + Retranslate.onLocaleOrLanguageChanged
                    value: "sk"
                    selected: Database.language === value
                }
                onSelectedIndexChanged: {
                    if (Database.language != selectedValue){
                        Database.language = selectedValue
                        App.changeLanguage(selectedValue)
                    }
                }
            } // end of DropDown
            
            // Share text
            TextField {
                
                property string savedText: qsTr("Saved") + Retranslate.onLocaleOrLanguageChanged
                
                inputMode: TextFieldInputMode.Text
                text: Database.shareText
                hintText: qsTr("enter custom prefix text to share") + Retranslate.onLocaleOrLanguageChanged
                onTextChanging: {
                    if (focused) previewLabel.text = "<u>"+text+"</u>" + " " + previewLabel.fixText
                }
                onFocusedChanged: {
                    if (!focused){
                        if (Database.shareText != text.trim()){
                            Database.shareText = text.trim()
                            Helper.showToast(savedText)
                        }
                    }
                }
                input{
                    submitKeyFocusBehavior: SubmitKeyFocusBehavior.Lose
                    submitKey: SubmitKey.EnterKey
                    onSubmitted: {
                        if (Database.shareText != text.trim()){
                            Database.shareText = text.trim()
                            Helper.showToast(savedText)
                        }
                    }
                }
            }
            Label {
                property string fixText: "http://bit.ly/123 " + pane.sharedText
                id: previewLabel
                multiline: true
                horizontalAlignment: HorizontalAlignment.Center
                textStyle{
                    fontSize: FontSize.PointValue
                    fontSizeValue: 6
                }
                text: "<u>"+Database.shareText+"</u>" + " " + fixText
                textStyle.textAlign: TextAlign.Center
                textFormat: TextFormat.Html
            }
        
        } // end of root container
    
    
    } // end of ScrollView

}