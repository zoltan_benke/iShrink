import bb.cascades 1.3

Container {
    id: root
    layout: DockLayout{}
    
    background: Color.create("#f8f8f8")
    
    
    ImageView {
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center
        scalingMethod: ScalingMethod.AspectFit
        imageSource: "asset:///images/cover_icon.png"
        preferredWidth: 250
    }

}