import bb.cascades 1.3

Page {
    
    property bool active: false
    property alias titleText: titleBar.title
    default property alias contentItems: rootContainer.controls 
    
    titleBar: TitleHeader {
        id: titleBar
        scrollBehavior: TitleBarScrollBehavior.Sticky
    }
    
    Container {
        id: rootContainer
        background: Color.create("#f8f8f8")
        layout: DockLayout{}
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
    }
}
