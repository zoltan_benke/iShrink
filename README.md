# iShrink #

### Simple application to generate short url (multi servers) ###

Release notes
-
### v1.0.1.8 (`18.12.2016`)
- [FIXED] URL for plugins interface
### v1.0.1.7 (`04.03.2015`)
- [ADDED] Chinese language
### v1.0.1.6 (`07.06.2014`)
- [FIXED] Uninstalling plugins
- Minor UI changes
### v1.0.1.5 (`02.06.2014`)
- [NEW] Blue icons
- [FIXED] Trademark rules (BlackBerry)
- Minor fixes
### v1.0.1.4 (`30.05.2014`)
- Ability to set custom share text
- Minor fixes
### v1.0.1.1 (`28.05.2014`)
- Updated Czech translation
### v1.0.1.0 (`27.05.2014`)
- Initial version