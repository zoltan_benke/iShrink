/*
 * downloader.cpp
 *
 *  Created on: 18.5.2014
 *      Author: benecore
 */

#include "downloader.h"
#include <QDir>

Downloader::Downloader(PluginItem* item, QObject *parent) :
QObject(parent),
manager(0),
outputFile(0),
plugin(item),
progressToast(0)
{
    QDir dir;
    if (dir.exists("data/plugins")){
    }else{
        dir.mkpath("data/plugins");
    }
    manager = new QNetworkAccessManager(this);
}

Downloader::Downloader(PluginItem* item, SystemProgressToast* toast, QObject* parent) :
                        QObject(parent),
                        manager(0),
                        outputFile(0),
                        plugin(item),
                        progressToast(toast)
{
    QDir dir;
    if (dir.exists("data/plugins")){
    }else{
        dir.mkpath("data/plugins");
    }
    manager = new QNetworkAccessManager(this);
}

Downloader::~Downloader()
{
}


void Downloader::startDownload()
{
    plugin->setError(false);
    outputFile = new QFile(this);
    outputFile->setFileName("data/plugins/"+plugin->fileName().toString().trimmed());
    if (!outputFile->open(QIODevice::WriteOnly)){
        qDebug() << "Unable open file" << outputFile->errorString();
        return;
    }
    plugin->setDownloading(true);

    QUrl url("https://backend.devpda.sk/ishrink/request.php");
    url.addQueryItem("plugin", QFileInfo(*outputFile).fileName());

    QNetworkRequest req(url);

    QNetworkReply * reply = manager->get(req);
    reply->ignoreSslErrors();
    connect(reply, SIGNAL(finished()), this, SLOT(downloadFinished()));
    connect(reply, SIGNAL(downloadProgress(qint64,qint64)), this, SLOT(downloadProgress(qint64,qint64)));
    connect(reply, SIGNAL(readyRead()), this, SLOT(readyRead()));
    downloadTime.start();
}

void Downloader::readyRead()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
    if (reply){
        outputFile->write(reply->readAll());
    }
}

void Downloader::downloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
    int progress = (bytesReceived*100)/bytesTotal;
    double speed = bytesReceived * 1000.0 / downloadTime.elapsed();
    QString unit;
    if (speed < 1024) {
        unit = "bytes/sec";
    } else if (speed < 1024*1024) {
        speed /= 1024;
        unit = "kB/s";
    } else {
        speed /= 1024*1024;
        unit = "MB/s";
    }
    progressToast->setStatusMessage(QString("%1%").arg(QString::number(progress)));
    progressToast->setProgress(progress);
    //plugin->setProgress(progress);
    //plugin->setSpeed(QString::fromLatin1("%1 %2").arg(speed, 3, 'f', 1).arg(unit));
    //emit downloadProgress(progress, QString::fromLatin1("%1 %2").arg(speed, 3, 'f', 1).arg(unit));
}


void Downloader::downloadFinished()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
    if (!reply->error()){
        qDebug() << "DOWNLOAD FINISHED:" << outputFile->fileName();
        outputFile->flush();
        outputFile->close();
        plugin->setError(false);
        emit activate(outputFile->fileName(), plugin);
    }else{
        qDebug() << "ErrorCode:" << reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() << endl << \
                "ErrorString:" << reply->errorString() << endl << \
                "Reply:" << reply->readAll();
        plugin->setError(true);
    }
    emit finished();
    reply->deleteLater();
}
