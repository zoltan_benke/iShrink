/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "applicationui.hpp"

#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/LocaleHandler>
#include <bb/data/JsonDataAccess>
#include <bb/system/SystemToast>
#include <bb/cascades/Label>
#include <QDebug>
#include <QVariantMap>
#include <QVariantList>

using namespace bb::cascades;
using namespace bb::data;
using namespace bb::system;

ApplicationUI::ApplicationUI(bb::cascades::Application *app) :
                    		QObject(app)
{
	QTextCodec::setCodecForCStrings(QTextCodec::codecForName("utf-8"));

	/** Registration handler **/
	const QString uuid(QLatin1String("98cf04f1-6ec8-4913-a581-45b8514a08be"));

	_registrationHandler = new RegistrationHandler(uuid);
	_inviteDownload = new InviteToDownload(_registrationHandler->context());
	_invokeManager = new InvokeManager(this);
	connect(_invokeManager,
			SIGNAL(invoked(const bb::system::InvokeRequest&)),
			this, SLOT(onInvoke(const bb::system::InvokeRequest&)));

	// prepare the localization
	m_pTranslator = new QTranslator(this);
	m_pLocaleHandler = new LocaleHandler(this);

	//bool res = QObject::connect(m_pLocaleHandler, SIGNAL(systemLanguageChanged()), this, SLOT(onSystemLanguageChanged()));
	// This is only available in Debug builds
	//Q_ASSERT(res);
	// Since the variable is not used in the app, this is added to avoid a
	// compiler warning
	//Q_UNUSED(res);

	// initial load
	//onSystemLanguageChanged();
	changeLanguage(Database::instance()->language());

	qmlRegisterType<PluginItem>();
	qmlRegisterType<LinkItem>("com.devpda.items", 1, 3, "LinkItem");
	qmlRegisterType<Invoker>("com.devpda.tools", 1, 3, "Invoker");
	// Create scene document from main.qml asset, the parent is set
	// to ensure the document gets destroyed properly at shut down.
	QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this)
			                                                                                            		   .property("App", this)
			                                                                                            		   .property("Database", Database::instance())
			                                                                                            		   .property("PluginControl", PluginControl::instance())
			                                                                                            		   .property("Helper", Helper::instance())
			                                                                                            		   .property("BBM", _registrationHandler)
			                                                                                            		   .property("Invite", _inviteDownload);


	QString workDir = QDir::homePath();
	QDeclarativePropertyMap* linksPaths = new QDeclarativePropertyMap(this);
	linksPaths->insert("jsonFile", QVariant(QString(
			"file://" + workDir + "/links.json")));
	linksPaths->insert("dbPath", QVariant(QString("file://" + workDir + "/database.db")));
	qml->setContextProperty("Links", linksPaths);

	// Create root object for the UI
	AbstractPane *root = qml->createRootObject<AbstractPane>();

	shortUrlLabel = root->findChild<Label*>("shortUrlLabel");
	urlTextField = root->findChild<TextField*>("urlField");
	if (shortUrlLabel && urlTextField){
		qDebug() << "LABEL FOUND";
		PluginControl::instance()->setLabel(shortUrlLabel);
	}

	app->setCover(new Frame());
	// Set created root object as the application scene
	app->setScene(root);

	new QmlBeam(this);
}


ApplicationUI::~ApplicationUI()
{
	Database::destroy();
	PluginControl::destroy();
	Helper::destroy();
	delete _registrationHandler;
	delete _inviteDownload;
	delete _invokeManager;
}

void ApplicationUI::changeLanguage(const QString& language)
{
	QCoreApplication::instance()->removeTranslator(m_pTranslator);
	// Initiate, load and install the application translation files.
	QString locale_string = QLocale().name();
	QString file_name = QString("iShrink_%1").arg(language);
	if (m_pTranslator->load(file_name, "app/native/qm")) {
		QCoreApplication::instance()->installTranslator(m_pTranslator);
	}
}

void ApplicationUI::onSystemLanguageChanged()
{
	QCoreApplication::instance()->removeTranslator(m_pTranslator);
	// Initiate, load and install the application translation files.
	QString locale_string = QLocale().name();
	QString file_name = QString("iShrink_%1").arg(locale_string);
	if (m_pTranslator->load(file_name, "app/native/qm")) {
		QCoreApplication::instance()->installTranslator(m_pTranslator);
	}
}

void ApplicationUI::onInvoke(const bb::system::InvokeRequest& invoke)
{
	qDebug() << "Invocation URL" << invoke.uri().toString();
	urlTextField->setText(invoke.uri().toString());
}
