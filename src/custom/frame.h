/*
 * Frame.h
 *
 *  Created on: 16-10-2012
 *      Author: Igor Andruszkiewicz
 */

#ifndef FRAME_H
#define FRAME_H

#include <bb/cascades/SceneCover>
#include <bb/cascades/Container>
#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/Label>
#include <bb/cascades/ImageView>

using namespace ::bb::cascades;

class Frame: public SceneCover {
	Q_OBJECT
public:
	Frame();
	virtual ~Frame();

protected:
	void create();

private:
	Q_DISABLE_COPY(Frame)
	Container *mMainContainer;
	QmlDocument *qml;
};

#endif /* FRAME_H */
