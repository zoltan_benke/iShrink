/*
 * helper.h
 *
 *  Created on: 21.5.2014
 *      Author: benecore
 */

#ifndef HELPER_H_
#define HELPER_H_

#include <QObject>
#include <bb/system/Clipboard>
#include <bb/system/SystemToast>
#include <bb/cascades/Invocation>
#include <bb/cascades/Invocation>
#include <bb/cascades/InvokeQuery>

#include "invoker.h"

using namespace bb::system;
using namespace bb::cascades;

class Helper : public QObject
{
    Q_OBJECT
public:
    Helper(QObject *parent = 0);
    virtual ~Helper();

    static Helper *instance();
    static void destroy();


    Q_INVOKABLE
    void copyText(const QByteArray &text);
    Q_INVOKABLE
    void showToast(const QString &message);
    Q_INVOKABLE
    void openBrowser(const QString &url);
    Q_INVOKABLE
    void share(const QString &data);

signals:
    void textCopied();



private slots:
    void armed();


private:
    Q_DISABLE_COPY(Helper)
    static Helper *_instance;
    Invoker *invoker;
    SystemToast *toast;
    Invocation *_invocation;

};

#endif /* HELPER_H_ */
