/*
 * plugin.cpp
 *
 *  Created on: 20.5.2014
 *      Author: benecore
 */

#include "plugincontrol.h"

PluginControl *PluginControl::_instance = 0;

PluginControl::PluginControl(QObject *parent) :
                                                                                        QObject(parent),
                                                                                        _manager(0),
                                                                                        _loading(false),
                                                                                        _shortUrlLabel(0)
{
}

PluginControl::~PluginControl()
{
}

PluginControl* PluginControl::instance()
{
    if (!_instance)
        _instance = new PluginControl;
    return _instance;
}

void PluginControl::destroy()
{
    if (_instance){
        delete _instance;
        _instance = 0;
    }
}

QVariantList PluginControl::installed()
{
    QVariantList tempList;
    QList<PluginInterface*> list = loadedPlugins.values();
    foreach(const PluginInterface *interface, list){
        QVariantMap map;
        map.insert("text", interface->name());
        map.insert("site", interface->site());
        tempList.append(map);
    }
    return tempList;
}

void PluginControl::installPlugin(QObject *object)
{
    PluginItem *plugin = qobject_cast<PluginItem*>(object);
    SystemProgressToast *progresstoast = new SystemProgressToast(this);
    progresstoast->setState(SystemUiProgressState::Active);
    progresstoast->setPosition(SystemUiPosition::TopCenter);
    progresstoast->setAutoUpdateEnabled(true);
    progresstoast->setStatusMessage("0%");
    progresstoast->setProgress(0);
    progresstoast->setBody(tr("Installing %1").arg(plugin->name().toString()));
    progresstoast->show();
    QThread* thread = new QThread;
    Downloader *down = new Downloader(plugin, progresstoast);
    down->moveToThread(thread);
    connect(thread, SIGNAL(started()), down, SLOT(startDownload()));
    connect(down, SIGNAL(activate(QString, PluginItem*)), this, SLOT(activatePlugin(QString, PluginItem*)));
    connect(down, SIGNAL(finished()), thread, SLOT(quit()));
    connect(down, SIGNAL(finished()), down, SLOT(deleteLater()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    connect(down, SIGNAL(finished()), progresstoast, SLOT(cancel()));
    connect(down, SIGNAL(finished()), progresstoast, SLOT(deleteLater()));
    thread->start();
}

void PluginControl::updatePlugin(QObject* object)
{
    PluginItem *plugin = qobject_cast<PluginItem*>(object);
    QString filePath = QString("data/plugins/%1").arg(plugin->fileName().toString());
    if (QFile(filePath).exists()){
        qDebug() << "Uninstalling plugin:" << filePath;
        PluginInterface *inter = loadedPlugins.take(plugin->name().toString());
        qDebug() << "Interface deleteting:" << inter->name();
        delete inter;
        Q_ASSERT(QFile(filePath).remove());
        plugin->setInstalled(false);
        plugin->setUpdate(false);
        SystemProgressToast *progresstoast = new SystemProgressToast(this);
        progresstoast->setState(SystemUiProgressState::Active);
        progresstoast->setPosition(SystemUiPosition::TopCenter);
        progresstoast->setAutoUpdateEnabled(true);
        progresstoast->setStatusMessage("0%");
        progresstoast->setProgress(0);
        progresstoast->setBody(tr("Updating %1").arg(plugin->name().toString()));
        progresstoast->show();
        QThread* thread = new QThread;
        Downloader *down = new Downloader(plugin, progresstoast);
        down->moveToThread(thread);
        connect(thread, SIGNAL(started()), down, SLOT(startDownload()));
        connect(down, SIGNAL(activate(QString, PluginItem*)), this, SLOT(activatePlugin(QString, PluginItem*)));
        connect(down, SIGNAL(finished()), thread, SLOT(quit()));
        connect(down, SIGNAL(finished()), down, SLOT(deleteLater()));
        connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
        connect(down, SIGNAL(finished()), progresstoast, SLOT(cancel()));
        connect(down, SIGNAL(finished()), progresstoast, SLOT(deleteLater()));
        thread->start();
    }

}


void PluginControl::uninstallPlugin(QObject* object)
{
    PluginItem *plugin = qobject_cast<PluginItem*>(object);
    QString filePath = QString("data/plugins/%1").arg(plugin->fileName().toString());
    if (QFile(filePath).exists()){
        qDebug() << "Uninstalling plugin:" << filePath;
        PluginInterface *inter = loadedPlugins.take(plugin->name().toString());
        qDebug() << "Interface deleteting:" << inter->name();
        delete inter;
        if (QFile(filePath).remove()){
            plugin->setInstalled(false);
            plugin->setUpdate(false);
            emit installedChanged();
            Helper::instance()->showToast(tr("Plugin uninstalled"));
        }
    }
}


void PluginControl::setLabel(bb::cascades::Label* label)
{
    _shortUrlLabel = label;
}


void PluginControl::shortUrl(const QString& interfaceName, const QString& url)
{
    qDebug() << Q_FUNC_INFO;
    loadedPlugins.value(interfaceName)->shortUrl(url);
}

void PluginControl::urlFinished(const QByteArray response)
{
    //_shortUrlLabel->setText(response);
    emit urlDone(response);
}

void PluginControl::error(int errorCode, QString errorString)
{
    Q_UNUSED(errorString);
    QString message = tr("Server error: %1\nTry again later").arg(errorCode);
    Helper::instance()->showToast(message);
}

void PluginControl::activatePlugin(const QString& fileName, PluginItem *plugin)
{
    QFileInfo info(fileName);
    QPluginLoader *loader = new QPluginLoader(fileName, this);
    QObject *object = loader->instance();
    if (object){
        PluginInterface *interface = qobject_cast<PluginInterface*>(object);
        if (interface){
            loadedPlugins.insert(interface->name(), interface);
            connect(interface, SIGNAL(error(int,QString)), this, SLOT(error(int,QString)));
            connect(interface, SIGNAL(urlDone(QByteArray)), this, SLOT(urlFinished(QByteArray)));
            qDebug() << "PLUGIN ACTIVATED" << info.fileName() << interface->version();
            plugin->setProgress(0);
            plugin->setSpeed();
            plugin->setInstalled(true);
            emit installedChanged();
        }
    }
}

void PluginControl::finished(QNetworkReply* reply)
{
    if (!reply->error()){
        JsonDataAccess jda;
        QString result = reply->readAll();
        QVariantList list = jda.loadFromBuffer(result).toList();
        const int &pluginCount = Database::instance()->pluginCount();
        if (pluginCount < list.count()){
            Helper::instance()->showToast(tr("New servers available"));
        }
        if (pluginCount != list.count()){
            Database::instance()->setPluginCount(list.count());
        }
        foreach(const QVariant &plugin, list){
            PluginItem *obj = new PluginItem(plugin.toMap(), this);
            obj->setInstalled(QFile("data/plugins/"+obj->fileName().toString().trimmed()).exists());
            if (!loadedPlugins.isEmpty()){
                PluginInterface *interface = loadedPlugins.value(obj->name().toString());
                if (interface){
                    if (obj->version().toString() > interface->version()){
                        obj->setUpdate(true);
                    }
                }
            }
            emit pluginDone(obj);
        }
    }else{
        qDebug() << "ErrorCode:" << reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() << endl << \
                "ErrorString:" << reply->errorString() << endl << \
                "Reply:" << reply->readAll();
        emit serverError(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), reply->errorString());
    }
    setLoading(false);
    reply->deleteLater();
    _manager->deleteLater();
}


void PluginControl::getPlugins()
{
    setLoading(true);
    _manager = new QNetworkAccessManager(this);
    connect(_manager, SIGNAL(finished(QNetworkReply*)),
            this,
            SLOT(finished(QNetworkReply*)));

    QUrl url("https://backend.devpda.sk/ishrink/plugins.php");

    QNetworkRequest req(url);

    _manager->get(req)->ignoreSslErrors();
}


void PluginControl::loadPlugins()
{
    QStringList files = QDir("data/plugins").entryList(QDir::Files | QDir::NoDotAndDotDot);
    int idx = 1;
    foreach (const QString &fileName, files){
        QString filePath = "data/plugins/"+fileName;
        QPluginLoader *loader = new QPluginLoader(filePath);
        QObject *plugin = loader->instance();
        if (plugin){
            PluginInterface *interface = qobject_cast<PluginInterface*>(plugin);
            if (interface){
                loadedPlugins.insert(interface->name(), interface);
                connect(interface, SIGNAL(error(int,QString)), this, SLOT(error(int,QString)));
                connect(interface, SIGNAL(urlDone(QByteArray)), this, SLOT(urlFinished(QByteArray)));
                qDebug() << "Plugin loaded" << idx << "/" << files.count();
                delete loader;
            }
        }
        ++idx;
    }
}
