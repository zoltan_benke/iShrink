/*
 * Frame.cpp
 *
 *  Created on: 16-10-2012
 *      Author: Igor Andruszkiewicz
 */

#include "frame.h"
#include <QDebug>

Frame::Frame() :
SceneCover(this)
{
	create();
}


Frame::~Frame() {
	// TODO Auto-generated destructor stub
}


void Frame::create()
{
	/**
	 * Remember to set permission Run When Backgrounded in bar-descriptor.xml.
	 */
	qml = QmlDocument::create("asset:///Cover.qml").parent(this);
	mMainContainer = qml->createRootObject<Container>();

	// Set the content of Frame
	setContent(mMainContainer);

	// You can do it now or trigger the slot whne application is moved to baackground.
	// QObject::connect(Application::instance(), SIGNAL(thumbnail()), this, SLOT(update()));
	// and/or disable updates when it's in foreground
}
