/*
 * downloader.h
 *
 *  Created on: 18.5.2014
 *      Author: benecore
 */

#ifndef DOWNLOADER_H_
#define DOWNLOADER_H_

#include <QObject>
#include <QFile>
#include <QTime>
#include <QDir>
#include <QtNetwork>
#include <QString>
#include <bb/system/SystemProgressToast>
#include <bb/system/SystemUiResult>
#include "custom/helper.h"

#include "pluginitem.h"

using namespace bb::system;

class Downloader : public QObject
{
    Q_OBJECT
public:
    Downloader(PluginItem* item, QObject *parent = 0);
    Downloader(PluginItem* item, SystemProgressToast *toast, QObject *parent = 0);
    virtual ~Downloader();


signals:
    void activate(const QString &fileName, PluginItem *plugin);
    void finished();
    void downloadProgress(const int percent, const QString speed);


public Q_SLOTS:
    void startDownload();

private Q_SLOTS:
    void readyRead();
    void downloadProgress(qint64 bytesReceived, qint64 bytesTotal);
    void downloadFinished();

private:
    QNetworkAccessManager *manager;
    QTime downloadTime;
    QFile *outputFile;
    PluginItem *plugin;
    SystemProgressToast *progressToast;
};

#endif /* DOWNLOADER_H_ */
